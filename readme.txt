-----------------------------------------------
SMSscript v2.1 by Rick Berwick
-----------------------------------------------

Thank you for purchasing this script from SMSscript.com!

To add the script into your page, either use an include statement for PHP, or add the code from the index.php file into the page you would like to display the message sender on.

Edit the config.php file with your database details, the message limit per person per day.

Edit the header.php and footer.php files to change the look of the output page for the message sending script.

Upload the database in smsscript2.sql to your database server.

Upload the files and start using the script!

Good luck! Thanks again for the purchase!