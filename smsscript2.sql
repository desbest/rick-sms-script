-- 
-- Table structure for table `carriers`
-- 

CREATE TABLE `carriers` (
  `id` int(1) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `carriers`
-- 

INSERT INTO `carriers` VALUES (1, 'MTS (Russia)', '@sms.mts.ru');
INSERT INTO `carriers` VALUES (2, 'PGSM (Hungary)', '@sms.pgsm.hu');
INSERT INTO `carriers` VALUES (3, 'Telecom Italia Mobile (Italy)', '@posta.tim.it');
INSERT INTO `carriers` VALUES (4, 'Vodafone Omnitel (Italy)', '@vizzavi.it');
INSERT INTO `carriers` VALUES (5, 'Max.Mobil (Austria)', '@max.mail.at');
INSERT INTO `carriers` VALUES (6, 'Orange (United Kingdom)', '@omail.net');
INSERT INTO `carriers` VALUES (7, 'Tele Danmark Mobil (Denmark)', '@sms.tdk.dk');
INSERT INTO `carriers` VALUES (8, 'Sonofon (Denmark)', '@note.sonofon.dk');
INSERT INTO `carriers` VALUES (9, 'Telia Mobitel (Sweden)', '@sms.comviq.se');
INSERT INTO `carriers` VALUES (10, 'Comviq GSM (Sweden)', '@sms.comviq.se');
INSERT INTO `carriers` VALUES (11, 'Europolitan (Sweden)', '@europolitan.se');
INSERT INTO `carriers` VALUES (12, 'Telenor Mobil (Norway)', '@mobilpost.com');
INSERT INTO `carriers` VALUES (13, 'Netcom GSM (Norway)', '@sms.netcom.no');
INSERT INTO `carriers` VALUES (14, 'Plus GSM (Poland)', '@text.plusgsm.pl');
INSERT INTO `carriers` VALUES (15, 'D1 De TeMobil (Germany)', '@t-d1-sms.de');
INSERT INTO `carriers` VALUES (16, 'D2 Mannesmann Mobilefunk (Germany)', '@d2-message.de');
INSERT INTO `carriers` VALUES (17, 'E-Plus (Germany)', '.sms@eplus.de');
INSERT INTO `carriers` VALUES (18, 'Celcom (Malaysia)', '@sms.celcom.com.my');
INSERT INTO `carriers` VALUES (19, 'Mobitel (Tanzania)', '@sms.co.tz');
INSERT INTO `carriers` VALUES (20, 'Telecel (Portugal)', '@sms.telecel.pt');
INSERT INTO `carriers` VALUES (21, 'Optimus (Portugal)', '@sms.optimus.pt');
INSERT INTO `carriers` VALUES (22, 'TMN (Portugal)', '@mail.tmn.pt');
INSERT INTO `carriers` VALUES (23, 'LMT (Latvia)', '@smsmail.lmt.lv');
INSERT INTO `carriers` VALUES (24, 'UMC GSM (Ukraine)', '@sms.umc.com.ua');
INSERT INTO `carriers` VALUES (25, 'Kyivstar GSM (Ukraine)', '@2sms.kyivstar.net');
INSERT INTO `carriers` VALUES (26, 'Si.Mobil (Slovenia)', '@simobil.net');
INSERT INTO `carriers` VALUES (27, 'Mobitel (Slovenia)', '@linux.mobitel.si');
INSERT INTO `carriers` VALUES (28, 'Eurotel (Czech Republic)', '@sms.eurotel.cz');
INSERT INTO `carriers` VALUES (29, 'Cellis & LibanCell (Lebanon)', '@ens.jinny.com.lb');
INSERT INTO `carriers` VALUES (30, '3 River Wireless', '@sms.3rivers.net');
INSERT INTO `carriers` VALUES (31, 'ACS Wireless', '@paging.acswireless.com');
INSERT INTO `carriers` VALUES (32, 'Advantage Communications', '@advantagepaging.com');
INSERT INTO `carriers` VALUES (33, 'Airtouch Pagers', '@airtouch.net');
INSERT INTO `carriers` VALUES (34, 'AlphaNow', '@alphanow.net');
INSERT INTO `carriers` VALUES (35, 'Alltel', '@alltelmessage.com');
INSERT INTO `carriers` VALUES (36, 'Alltel PCS', '@message.alltel.com');
INSERT INTO `carriers` VALUES (37, 'Ameritech Paging', '@paging.acswireless.com');
INSERT INTO `carriers` VALUES (38, 'Ameritech Clearpath', '@clearpath.acswireless.com');
INSERT INTO `carriers` VALUES (39, 'Andhra Pradesh Airtel', '@airtelap.com');
INSERT INTO `carriers` VALUES (40, 'Arch Pagers (PageNet)', '@archwireless.net');
INSERT INTO `carriers` VALUES (41, 'AT&T PCS', '@mobile.att.net');
INSERT INTO `carriers` VALUES (42, 'AT&T Pocketnet PCS', '@dpcs.mobile.att.net');
INSERT INTO `carriers` VALUES (43, 'Beepwear', '@beepwear.net');
INSERT INTO `carriers` VALUES (44, 'BeeLine GSM', '@sms.beemail.ru');
INSERT INTO `carriers` VALUES (45, 'Bell Atlantic', '@message.bam.com');
INSERT INTO `carriers` VALUES (46, 'Bell Canada', '@txt.bellmobility.ca');
INSERT INTO `carriers` VALUES (47, 'Bell Mobility (Canada)', '@txt.bell.ca');
INSERT INTO `carriers` VALUES (48, 'Bell Mobility', '@txt.bellmobility.ca');
INSERT INTO `carriers` VALUES (49, 'Bell South (Blackberry)', '@bellsouthtips.com');
INSERT INTO `carriers` VALUES (50, 'Bell South', '@sms.bellsouth.com');
INSERT INTO `carriers` VALUES (51, 'Bell South Mobility', '@blsdcs.net');
INSERT INTO `carriers` VALUES (52, 'Blue Sky Frog', '@blueskyfrog.com');
INSERT INTO `carriers` VALUES (53, 'Bluegrass Cellular', '@sms.bluecell.com');
INSERT INTO `carriers` VALUES (54, 'Boost', '@myboostmobile.com');
INSERT INTO `carriers` VALUES (55, 'BPL mobile', '@bplmobile.com');
INSERT INTO `carriers` VALUES (56, 'Carolina Mobile Communications', '@cmcpaging.com');
INSERT INTO `carriers` VALUES (57, 'Cellular One East Coast', '@phone.cellone.net');
INSERT INTO `carriers` VALUES (58, 'Cellular One South West', '@swmsg.com');
INSERT INTO `carriers` VALUES (59, 'Cellular One PCS', '@paging.cellone-sf.com');
INSERT INTO `carriers` VALUES (60, 'Cellular One', '@mobile.celloneusa.com');
INSERT INTO `carriers` VALUES (61, 'Cellular One West', '@mycellone.com');
INSERT INTO `carriers` VALUES (62, 'Cellular South', '@csouth1.com');
INSERT INTO `carriers` VALUES (63, 'Central Vermont Communications', '@cvcpaging.com');
INSERT INTO `carriers` VALUES (64, 'CenturyTel', '@messaging.centurytel.net');
INSERT INTO `carriers` VALUES (65, 'Chennai RPG Cellular', '@rpgmail.net');
INSERT INTO `carriers` VALUES (66, 'Chennai Skycell / Airtel', '@airtelchennai.com');
INSERT INTO `carriers` VALUES (67, 'Cincinnati Bell', '@mobile.att.net');
INSERT INTO `carriers` VALUES (68, 'Cingular', '@cingularme.com');
INSERT INTO `carriers` VALUES (69, 'Cingular Wireless', '@mobile.mycingular.com');
INSERT INTO `carriers` VALUES (70, 'Clearnet', '@msg.clearnet.com');
INSERT INTO `carriers` VALUES (71, 'Comcast', '@comcastpcs.textmsg.com');
INSERT INTO `carriers` VALUES (72, 'Communication Specialists', '@pageme.comspeco.net');
INSERT INTO `carriers` VALUES (73, 'Comviq', '@sms.comviq.se');
INSERT INTO `carriers` VALUES (74, 'Cook Paging', '@cookmail.com');
INSERT INTO `carriers` VALUES (75, 'Corr Wireless Communications', '@corrwireless.net');
INSERT INTO `carriers` VALUES (76, 'Delhi Aritel', '@airtelmail.com');
INSERT INTO `carriers` VALUES (77, 'Digi-Page / Page Kansas', '@page.hit.net');
INSERT INTO `carriers` VALUES (78, 'Dobson Cellular Systems', '@mobile.dobson.net');
INSERT INTO `carriers` VALUES (79, 'Dobson-Alex Wireless / Dobson-Cellular One', '@mobile.cellularone.com');
INSERT INTO `carriers` VALUES (80, 'DT T-Mobile', '@t-mobile-sms.de');
INSERT INTO `carriers` VALUES (81, 'Dutchtone / Orange-NL', '@sms.orange.nl');
INSERT INTO `carriers` VALUES (82, 'Edge Wireless', '@sms.edgewireless.com');
INSERT INTO `carriers` VALUES (83, 'EMT', '@sms.emt.ee');
INSERT INTO `carriers` VALUES (84, 'Escotel', '@escotelmobile.com');
INSERT INTO `carriers` VALUES (85, 'Fido', '@fido.ca');
INSERT INTO `carriers` VALUES (86, 'Galaxy Corporation', '.epage@sendabeep.net');
INSERT INTO `carriers` VALUES (87, 'GCS Paging', '@webpager.us');
INSERT INTO `carriers` VALUES (88, 'Goa BPLMobil', '@bplmobile.com');
INSERT INTO `carriers` VALUES (89, 'Golden Telecom', '@sms.goldentele.com');
INSERT INTO `carriers` VALUES (90, 'GrayLink / Porta-Phone', '@epage.porta-phone.com');
INSERT INTO `carriers` VALUES (91, 'GTE', '@airmessage.net');
INSERT INTO `carriers` VALUES (92, 'Gujarat Celforce', '@celforce.com');
INSERT INTO `carriers` VALUES (93, 'Houston Cellular', '@text.houstoncellular.net');
INSERT INTO `carriers` VALUES (94, 'Infopage Systems', '@page.infopagesystems.com');
INSERT INTO `carriers` VALUES (95, 'Inland Cellular Telephone', '@inlandlink.com');
INSERT INTO `carriers` VALUES (96, 'JSM Tele-Page', '@jsmtel.com');
INSERT INTO `carriers` VALUES (97, 'Kerala Escotel', '@escotelmobile.com');
INSERT INTO `carriers` VALUES (98, 'Kolkata Airtel', '@airtelkol.com');
INSERT INTO `carriers` VALUES (99, 'Kyivstar', '@smsmail.lmt.lv');
INSERT INTO `carriers` VALUES (100, 'Lauttamus Communication', '@e-page.net');
INSERT INTO `carriers` VALUES (101, 'LMT', '@smsmail.lmt.lv');
INSERT INTO `carriers` VALUES (102, 'Maharashtra BPL Mobile', '@bplmobile.com');
INSERT INTO `carriers` VALUES (103, 'Maharashtra Idea Cellular', '@ideacellular.net');
INSERT INTO `carriers` VALUES (104, 'Manitoba Telecom Systems', '@text.mtsmobility.com');
INSERT INTO `carriers` VALUES (105, 'MCI Phone', '@mci.com');
INSERT INTO `carriers` VALUES (106, 'MCI Page', '@pagemci.com');
INSERT INTO `carriers` VALUES (107, 'Meteor', '@sms.mymeteor.ie');
INSERT INTO `carriers` VALUES (108, 'Metrocall', '@page.metrocall.com');
INSERT INTO `carriers` VALUES (109, 'Metrocall 2-way', '@my2way.com');
INSERT INTO `carriers` VALUES (110, 'Metro PCS', '@mymetropcs.com');
INSERT INTO `carriers` VALUES (111, 'Microcell', '@fido.ca');
INSERT INTO `carriers` VALUES (112, 'Midwest Wireless', '@clearlydigital.com');
INSERT INTO `carriers` VALUES (113, 'MiWorld', '@m1.com.sg');
INSERT INTO `carriers` VALUES (114, 'Mobilecom PA', '@page.mobilcom.net');
INSERT INTO `carriers` VALUES (115, 'Mobilecomm', '@mobilecomm.net');
INSERT INTO `carriers` VALUES (116, 'Mobileone', '@m1.com.sg');
INSERT INTO `carriers` VALUES (117, 'Mobilfone', '@page.mobilfone.com');
INSERT INTO `carriers` VALUES (118, 'Mobility Bermuda', '@ml.bm');
INSERT INTO `carriers` VALUES (119, 'Mobistar Belgium', '@mobistar.be');
INSERT INTO `carriers` VALUES (120, 'Mobitel Tanzania', '@sms.co.tz');
INSERT INTO `carriers` VALUES (121, 'Mobtel Srbija', '@mobtel.co.yu');
INSERT INTO `carriers` VALUES (122, 'Morris Wireless', '@beepone.net');
INSERT INTO `carriers` VALUES (123, 'Motient', '@isp.com');
INSERT INTO `carriers` VALUES (124, 'Movistar', '@correo.movistar.net');
INSERT INTO `carriers` VALUES (125, 'Mumbai BPL Mobile', '@bplmobile.com');
INSERT INTO `carriers` VALUES (126, 'Mumbai Orange', '@orangemail.co.in');
INSERT INTO `carriers` VALUES (127, 'NBTel', '@wirefree.informe.ca');
INSERT INTO `carriers` VALUES (128, 'Netcom', '@sms.netcom.no');
INSERT INTO `carriers` VALUES (129, 'NPI Wireless', '@npiwireless.com');
INSERT INTO `carriers` VALUES (130, 'Ntelos', '@pcs.ntelos.com');
INSERT INTO `carriers` VALUES (131, 'O2', '@o2.co.uk');
INSERT INTO `carriers` VALUES (132, 'O2 (M-mail)', '@mmail.co.uk');
INSERT INTO `carriers` VALUES (133, 'Omnipoint', '@omnipoint.com');
INSERT INTO `carriers` VALUES (134, 'One Connect Austria', '@onemail.at');
INSERT INTO `carriers` VALUES (135, 'OnlineBeep', '@onlinebeep.net');
INSERT INTO `carriers` VALUES (136, 'Optus Mobile', '@optusmobile.com.au');
INSERT INTO `carriers` VALUES (137, 'Orange', '@orange.net');
INSERT INTO `carriers` VALUES (138, 'Orange Mumbai', '@orangemail.co.in');
INSERT INTO `carriers` VALUES (139, 'Orange - NL / Dutchtone', '@sms.orange.nl');
INSERT INTO `carriers` VALUES (140, 'Oskar', '@mujoskar.cz');
INSERT INTO `carriers` VALUES (141, 'P&T Luxembourg', '@sms.luxgsm.lu');
INSERT INTO `carriers` VALUES (142, 'Pacific Bell', '@pacbellpcs.net');
INSERT INTO `carriers` VALUES (143, 'PageMart', '@pagemart.net');
INSERT INTO `carriers` VALUES (144, 'PageMart Advanced /2way', '@airmessage.net');
INSERT INTO `carriers` VALUES (145, 'PageMart Canada', '@pmcl.net');
INSERT INTO `carriers` VALUES (146, 'PageNet Canada', '@pagegate.pagenet.ca');
INSERT INTO `carriers` VALUES (147, 'PageOne NorthWest', '@page1nw.com');
INSERT INTO `carriers` VALUES (148, 'PCS One', '@pcsone.net');
INSERT INTO `carriers` VALUES (149, 'Personal Communication', 'sms@pcom.ru');
INSERT INTO `carriers` VALUES (150, 'Pioneer / Enid Cellular', '@msg.pioneerenidcellular.com');
INSERT INTO `carriers` VALUES (151, 'PlusGSM', '@text.plusgsm.pl');
INSERT INTO `carriers` VALUES (152, 'Pondicherry BPL Mobile', '@bplmobile.com');
INSERT INTO `carriers` VALUES (153, 'Powertel', '@voicestream.net');
INSERT INTO `carriers` VALUES (154, 'Price Communications', '@mobilecell1se.com');
INSERT INTO `carriers` VALUES (155, 'Primco', '@primeco.extmsg.com');
INSERT INTO `carriers` VALUES (156, 'Primtel', '@sms.primtel.ru');
INSERT INTO `carriers` VALUES (157, 'ProPage', '@page.propage.net');
INSERT INTO `carriers` VALUES (158, 'Public Service Cellular', '@sms.pscel.com');
INSERT INTO `carriers` VALUES (159, 'Qualcomm', '@pager.qualcomm.com');
INSERT INTO `carriers` VALUES (160, 'Qwest', '@qwestmp.com');
INSERT INTO `carriers` VALUES (161, 'RAM Page', '@ram-page.com');
INSERT INTO `carriers` VALUES (162, 'Rogers AT&T Wireless', '@pcs.rogers.com');
INSERT INTO `carriers` VALUES (163, 'Rogers Canada', '@pcs.rogers.com');
INSERT INTO `carriers` VALUES (164, 'Safaricom', '@safaricomsms.com');
INSERT INTO `carriers` VALUES (165, 'Satelindo GSM', '@satelindogsm.com');
INSERT INTO `carriers` VALUES (166, 'Satellink', '.pageme@satellink.net');
INSERT INTO `carriers` VALUES (167, 'SBC Ameritech Paging', '@paging.acswireless.com');
INSERT INTO `carriers` VALUES (168, 'SCS-900', '@scs-900.ru');
INSERT INTO `carriers` VALUES (169, 'SFR France', '@sfr.fr');
INSERT INTO `carriers` VALUES (170, 'Skytel Pagers', '@skytel.com');
INSERT INTO `carriers` VALUES (171, 'Simple Freedom', '@text.simplefreedom.net');
INSERT INTO `carriers` VALUES (172, 'Smart Telecom', '@mysmart.mymobile.ph');
INSERT INTO `carriers` VALUES (173, 'Southern LINC', '@page.southernlinc.com');
INSERT INTO `carriers` VALUES (174, 'Southwestern Bell', '@email.swbw.com');
INSERT INTO `carriers` VALUES (175, 'Sprint', '@sprintpaging.com');
INSERT INTO `carriers` VALUES (176, 'Sprint PCS', '@messaging.sprintpcs.com');
INSERT INTO `carriers` VALUES (177, 'ST Paging', '@page.stpaging.com');
INSERT INTO `carriers` VALUES (178, 'SunCom', '@tms.suncom.com');
INSERT INTO `carriers` VALUES (179, 'Sunrise Mobile', '@mysunrise.ch');
INSERT INTO `carriers` VALUES (180, 'Surewest Communicaitons', '@mobile.surewest.com');
INSERT INTO `carriers` VALUES (181, 'Swisscom', '@bluewin.ch');
INSERT INTO `carriers` VALUES (182, 'T-Mobile / Voicestream', '@tmomail.net');
INSERT INTO `carriers` VALUES (183, 'T-Mobile Austria', '@sms.t-mobile.at');
INSERT INTO `carriers` VALUES (184, 'T-Mobile Germany', '@t-d1-sms.de');
INSERT INTO `carriers` VALUES (185, 'T-Mobile UK', '@t-mobile.uk.net');
INSERT INTO `carriers` VALUES (186, 'Tamil Nadu BPL Mobile', '@bplmobile.com');
INSERT INTO `carriers` VALUES (187, 'Tele2 Latvia', '@sms.tele2.lv');
INSERT INTO `carriers` VALUES (188, 'Telefonica Movistar', '@movistar.net');
INSERT INTO `carriers` VALUES (189, 'Telenor', '@mobilpost.no');
INSERT INTO `carriers` VALUES (190, 'Teletouch', '@pageme.teletouch.com');
INSERT INTO `carriers` VALUES (191, 'Telia Denmark', '@gsm1800.telia.dk');
INSERT INTO `carriers` VALUES (192, 'Telus', '@msg.telus.com');
INSERT INTO `carriers` VALUES (193, 'TIM', '@timnet.com');
INSERT INTO `carriers` VALUES (194, 'Triton', '@tms.suncom.com');
INSERT INTO `carriers` VALUES (195, 'TSR Wireless', '@alphame.com');
INSERT INTO `carriers` VALUES (196, 'UMC', '@sms.umc.com.ua');
INSERT INTO `carriers` VALUES (197, 'Unicel', '@utext.com');
INSERT INTO `carriers` VALUES (198, 'Uraltel', '@sms.uraltel.ru');
INSERT INTO `carriers` VALUES (199, 'US Cellular', '@email.uscc.net');
INSERT INTO `carriers` VALUES (200, 'US West', '@uswestdatamail.com');
INSERT INTO `carriers` VALUES (201, 'Uttar Pradesh Escotel', '@escotelmobile.com');
INSERT INTO `carriers` VALUES (202, 'Verizon Pagers', '@myairmail.com');
INSERT INTO `carriers` VALUES (203, 'Verizon PCS', '@vtext.com');
INSERT INTO `carriers` VALUES (204, 'Vessotel', '@pager.irkutsk.ru');
INSERT INTO `carriers` VALUES (205, 'Virgin Mobile', '@vmobl.com');
INSERT INTO `carriers` VALUES (206, 'Vodafone Italy', '@sms.vodafone.it');
INSERT INTO `carriers` VALUES (207, 'Vodafone Japan', '@c.vodafone.ne.jp');
INSERT INTO `carriers` VALUES (208, 'Vodafone Spain', '@vodafone.es');
INSERT INTO `carriers` VALUES (209, 'Vodafone UK', '@vodafone.net');
INSERT INTO `carriers` VALUES (210, 'WebLink Wiereless', '@airmessage.net');
INSERT INTO `carriers` VALUES (211, 'West Central Wireless', '@sms.wcc.net');
INSERT INTO `carriers` VALUES (212, 'Western Wireless', '@cellularonewest.com');
INSERT INTO `carriers` VALUES (213, 'Wyndtell', '@wyndtell.com');



-- --------------------------------------------------------

-- 
-- Table structure for table `users`
-- 

CREATE TABLE `users` (
  `ip` varchar(255) NOT NULL default '',
  `to` varchar(255) NOT NULL default '',
  `day` varchar(255) NOT NULL default ''
) TYPE=MyISAM;
